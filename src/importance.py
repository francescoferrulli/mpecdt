import numpy as np
import pylab 
import matplotlib.pyplot as plt
"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return x**2

def Exercise2(x):
    """
    Function For the second exercise. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return np.exp(-10.0 * x)* np.cos(x)

def normal(N):
    """
    Function to return a numpy array containing N samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)
    
def uniform(N):
    """
    Function to return a numpy array containing N samples from 
    a uniform distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    #return np.linspace(0.0,0.99999,N)
    return np.random.rand(N)
    
def cluster(N):
    """
    Function to return a numpy array containing N samples from 
    a cluster distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    #u= np.linspace(0.0,0.99999,N)
    u= np.random.rand(N)
    cl= -0.1 * np.log(1-u+ u*np.exp(-10))
    return cl

def normal_pdf(x):
    """
    Function to evaluate the PDF for the normal distribution (the
    normalisation coefficient is ignored). If the input is a numpy
    array, this returns a numpy array with the PDF evaluated at each
    of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    return np.exp(-x**2/2)

def uniform_pdf(x):
    """
    Function to evaluate the PDF for the standard uniform
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    y = 0*x
    y[x>0] = 1.0
    y[x>1] = 0.0
    return y
    
def cluster_pdf(x):
    """
    Function to evaluate the PDF for the cluster
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    y = 0*x
    y[x>0] = 10.0 * np.exp(-10*x) / (1 - np.exp(-10))
    y[x>1] = 0.0
    return y

def rough_estimator(f,Y,N):
    """
    Estimator for the function f which use the uniform distribution.
    """
    y=Y(N)
    fy= f(y)
    theta = np.sum(fy)/N
    return theta
    

def importance(f, Y, rho, rhoprime, N):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    """
    y= Y(N)
    fy=f(y)
    rhoy= rho(y)
    rhoprimey = rhoprime(y)
    theta = np.sum(fy * (rhoy / rhoprimey)) *pow(np.sum(rhoy/rhoprimey),-1)

    return theta

if __name__ == '__main__':
    #just a preliminary test
    theta = importance(square,normal,uniform_pdf,normal_pdf,30)
    print "theta = ", theta, 1.0/3.0 -theta
    
    
    #Let's see what happens if we make more accurate calculations, i.e. increasing N
    
    K=np.linspace(0.5,5,300)
    N= 10**K
    Err=np.zeros(len(N))
    for i in range(len(N)):
        Err[i]=abs(1.0/3.0 - importance(square,normal,uniform_pdf,normal_pdf,N[i]))
    plt.loglog(N,Err, 'r')
    plt.show()


    
    Err2=np.zeros(len(N))
    Err2low=np.zeros(len(N))
    Analityc_value = 10.0/101.0 - (10.0*np.cos(1) - np.sin(1))/ (101.0 * np.exp(10))
    
    for i in range(len(N)):
        Err2low[i]=abs(Analityc_value - rough_estimator(Exercise2,uniform,N[i]))
        Err2[i]=abs(Analityc_value - importance(Exercise2,cluster,uniform_pdf,cluster_pdf,N[i]))
    

    plt.loglog(N,Err2low, 'g', label="Equispaced points method")
    plt.loglog(N,Err2, 'b', label="Clustered distribution")
    plt.legend( loc=3)
    plt.show()
    
    
    
    


