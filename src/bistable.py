import pylab
import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt

def Vprime_bistable(x):
    return x**3-x

def brownian_dynamics(x,Vp,sigma,dt,T,tdump):
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW

        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
    return np.array(xvals), np.array(tvals)
    
def invariant_density(x,sigma=0.5):
    """
    This function will return the exact value of the density for the invariant measure
    """
    func = lambda x: np.exp(-2.0/(sigma**2)* ((x**4)/4 - (x**2)/2))
    Int= integrate.quad(func,-np.inf, np.inf)[0]
    density = lambda x: np.exp(-2.0/(sigma**2)* ((x**4)/4 - (x**2)/2))/Int
    return density(x)
    
    
def error_time_approx_x(t,f):
    numerical_x= sum(f)/t
    func_x = lambda x: x*invariant_density(x)
    Int_x = integrate.quad(func_x,-np.inf, np.inf)[0]
    return abs(numerical_x-Int_x)
    
def error_time_approx_xSquare(t,f):
    numerical_xS= sum(f**2)/t
    func_xS = lambda x: (x**2)*invariant_density(x)
    Int_xS = integrate.quad(func_xS,-np.inf, np.inf)[0]
    return abs(numerical_xS-Int_xS)
    
    
    
    
if __name__ == '__main__':
    dt = 0.1
    sigma =0.5
    T = 100.0

    """
    ---Question 2 ---
    We will assume N be the number of the realisation of the sPDE
    We will then consider a matix X where the columns will be the different realisations 
    given by the invariant_density function, built up by a for-cycle.
    
    We will plot then the histograms for times T = 10,50,100,300,500,1000
    and we will see that starting from T=300 the histograms will show the same qualitative properties
    the real density has, namely the double wells.
    
    
    """

    N= 1000 #number of samples
    X=np.zeros((T/dt + 1,N))
    Time=np.zeros(T/dt + 1)

    Density = invariant_density(np.linspace(-2.5, 2.5 ,T/dt)) #Samples for density's plot
    for i in range(N):
        X[:,i],Time = brownian_dynamics(1.0,Vprime_bistable,sigma,dt,T,dt)
    
    pylab.hist(X[10,:], bins=50, normed=1, facecolor='y', label='Distribution at t= 10')
    pylab.plot(np.linspace(-2.5, 2.5 ,T/dt), Density, 'b', label='Exact density')
    plt.legend( loc=0)
    pylab.show()
    
    pylab.hist(X[50,:], bins=50, normed=1, facecolor='y', label='Distribution at t= 50')
    pylab.plot(np.linspace(-2.5, 2.5 ,T/dt), Density, 'b', label='Exact density')
    plt.legend( loc=0)
    pylab.show()
    
    pylab.hist(X[100,:], bins=50, normed=1, facecolor='y', label='Distribution at t= 100')
    pylab.plot(np.linspace(-2.5, 2.5 ,T/dt), Density, 'b', label='Exact density')
    plt.legend( loc=0)
    pylab.show()
    
    pylab.hist(X[300,:], bins=50, normed=1, facecolor='y', label='Distribution at t= 300')
    pylab.plot(np.linspace(-2.5, 2.5 ,T/dt), Density, 'b', label='Exact density')
    plt.legend( loc=0)
    pylab.show()
    
    pylab.hist(X[500,:], bins=50, normed=1, facecolor='y', label='Distribution at t= 500')
    pylab.plot(np.linspace(-2.5, 2.5 ,T/dt), Density, 'b', label='Exact density')
    plt.legend( loc=0)
    pylab.show()
    
    pylab.hist(X[1000,:], bins=50, normed=1, facecolor='y', label='Distribution at t= 1000')
    pylab.plot(np.linspace(-2.5, 2.5 ,T/dt), Density, 'b', label='Exact density')
    plt.legend( loc=0)
    pylab.show()
    
    """
    --- Question 3 ---
    Now we will construct only one realisation and we will analise the behavoiur on time.
    We will consider 200 captures on time and we will plot the the absolute value of the error.
    We will see thanks to a y-semilog rappresentation that the method seems to converge with an exponential rate
    even if the plot presents many humps due to the randomness of the method.
    TO BE CONTINUED
     
    """
    T2=10**3
    x,t = brownian_dynamics(1.0,Vprime_bistable,sigma,dt,T2,dt) #x and t are vector of shape (T2*10,1)

    
    captions=200
    vals_x=np.zeros(captions)
    vals_xS=np.zeros(captions)
    time_step=np.linspace(0.0,T2*10,captions)  #T2*10 because of dt=0.1
    for j in range(captions):
        vals_x[j]= error_time_approx_x(time_step[j],x[:time_step[j]])
        vals_xS[j]= error_time_approx_xSquare(time_step[j],x[:time_step[j]])
    
    pylab.plot(time_step,vals_x)
    pylab.xlabel('time')
    pylab.ylabel('approximated values')
    pylab.show()
    pylab.semilogy(time_step,vals_x)
    pylab.xlabel('time ')
    pylab.ylabel('Error on log scale')
    pylab.show()
    
    pylab.plot(time_step,vals_xS)
    pylab.xlabel('time')
    pylab.ylabel('approximated values')
    pylab.show()
    pylab.semilogy(time_step,vals_xS)
    pylab.xlabel('time ')
    pylab.ylabel('Error on log scale')
    pylab.show()

