from scipy import integrate
import numpy as np
import pylab

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

You should fork this repository and add any extra functions and
classes to this module.
"""

def vectorfield(y, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    args - an object containing parameters

    Outputs:
    dxdt - the value of dx/dt
    """

    return sigma*(y[1]-y[0]), y[0]*(rho-y[2]) - y[1] ,    y[0]*y[1] - beta*y[2]

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10,28,8/3))
    return data, t  

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()
    
def predict_linear(x,time):
	y= 1.0 * x
	for n in range(len(x)-3):
		y[n+2]=2* x[n+1] - x[n]
	RMSerror = np.average((y[2:]-x[2:])**2)**0.5 
	print "RMS error for linear forecasting is:", RMSerror
	
	error = np.sqrt(sum(np.power(2,np.abs(y-x)))/(len(x)-3))
	print'err',error
	
	pylab.figure()
	pylab.plot(time[2:],y[2:],'.',
	           time[2:],x[2:],'-')
	pylab.legend(('Forecast','Observation'))
	pylab.xlabel('Time')
	pylab.ylabel('x component')
	pylab.title('Linear forecast one step ahead')

def predict_linear_steps(x,time,steps):
	y= np.zeros(shape=(len(x),steps+1))
	for n in range(len(x)):
		y[n,0]=x[n]
		for i in range(1,steps+1):
			for n in range(len(x)-3):
				y[n+2,i]=2* y[n+1,i-1] - y[n,i-1]
	ystep= y[:,steps]
	
	#RMSerror = np.average((ystep[steps+1:]-x[steps+1:])**2)**0.5
	RMSerror = np.average((ystep-x)**2)**0.5
	print RMSerror
	
	pylab.figure()
	pylab.plot(time[steps+1:],y[steps+1:],'.',
	           time[steps+1:],x[steps+1:],'-')
	
	
			
			
			
#a=[0,1,2,3,4,5]
#b= np.zeros(shape=(5,6))
#for i in range(3):
	#b[i]=a
#print b

#c= np.zeros(shape=(7,3))
#for i in range(3):
	#for j in range(7):
		#c[j,i]=i+j
#print c






            
if __name__ == '__main__':
    mydat, mytime = getdata([-0.587,-0.563,16.870],100.0,0.05)

    plot3D(mydat[:,0],mydat[:,1],mydat[:,2])


predict_linear(mydat[:,0],mytime)
predict_linear_steps(mydat[:,0],mytime,2)
pylab.show()


		    
