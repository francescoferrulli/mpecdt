import numpy as np
import pylab as pl

def lmap(x):
   """
   Function to evaluate y = a*x + b for a = 2, b = 1

   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the double well polynomial evaluated on x
   """
   a = 2
   b = 1
   return a*x + b

def myprior():
   """
   Function to draw a sample from a prior distribution given by the
   normal distribution with variance alpha.
   inputs: none
   outputs: the sample
   """
   alpha = 1.0
   return alpha**0.5*np.random.randn()

def Phi(f,x,y):
   """
   Function to return Phi, assuming a normal distribution for 
   the observation noise, with mean 0 and variance sigma.
   inputs:
   f - the function implementing the forward model
   x - a value of the model state x
   y - a value of the observed data
   """
   sigma = 1.0e-1
   return (f(x)-y)**2/(sigma**2 *2)  #I have added 2 at the denominator

def mcmc(f,prior_sampler,x0,yhat,N,beta):
   """
   Function to implement the MCMC pCN algorithm.
   inputs:
   f - the function implementing the forward model
   prior_sampler - a function that generates samples from the prior
   x0 - the initial condition for the Markov chain
   yhat - the observed data
   N - the number of samples in the chain
   beta - the pCN parameter

   outputs:
   xvals - the array of sample values
   avals - the array of acceptance probabilities
   """
   xvals = np.zeros(N+1)
   xvals[0] = x0
   avals = np.zeros(N)

   for i in range(N):
      w = prior_sampler()
      prop = (1- beta**2)**0.5 * xvals[i] + beta*w
      avals[i] = min(1, np.exp( Phi(f,xvals[i],yhat) - Phi(f,prop,yhat)))
      U = np.random.uniform()
      
      if U < avals[i]:
         xvals[i+1] = prop
      else:
         xvals[i+1] = xvals[i]

   return xvals,avals

def partial_sum_aver(y):
   """
   This function will compute the cumulative average over the components of y
   that in our case will be the vector avals[:] 
   """
   s= np.zeros(len(y))
   s = np.cumsum(y)/np.arange(1,len(y)+1)
   
   return s
   

def iden(x):
   return x

def expon(x):
   return np.exp(x)
   
def square(x):
   return x**2

if __name__ == '__main__':
   
     ##
   ###### EXERCISE 1
     ##
     
   yhat = 0.5
   x0 = 0.5  
   
   xvals,avals = mcmc(iden,myprior,x0,yhat,1000,0.5)
   
   
   cumul = partial_sum_aver(avals)
   pl.plot(range(len(cumul)), cumul)
   pl.title('Ex.1: Partial average against number of proposals')
   pl.show()
   print("The average value reached after %f proposal is %f." %(int(len(cumul)-1),cumul[len(cumul)-1]))
   print("In this case even without tuning beta we reached the optimal value for the average which has been predicted theoretically to be close to 0.25.")
   print("After many simulations we may notice that the average is quite close to the optimal value after 400 simulation.")
   print("We will apply a burning procedure to the algorithm. We will discard so the first 399 simulation and we will compute the average and variance of the rest of them.")
   
   new_xvals = xvals[400:]
   mean_new = np.mean(new_xvals)
   variance_new = np.var(new_xvals)
   
   print("Ex.1: The theoretical values for mean and average are respectively 0.5 and 0.5. The values obtained are %f %f"%(mean_new,variance_new) )
   
   pl.hist(new_xvals, bins=75, normed=1)
   pl.title('PDF for the valuse taken after discarding the first 400 simulation.')
   pl.show()
   print('As we can see, even if the value of thevariance seems to do not match the theoretical expectaions, the last plot showed a quite accordance of the distribution with the normal law.')
   
     ##
   ###### EXERCISE 2
     ##
   """   This second part has a new data setup   """
   yhat=1.0
   
   """Let's do again what did for exercise 1"""
   
   """Case beta = 0.1 """
   xvals,avals = mcmc(expon,myprior,x0,yhat,1000,0.1)
   
   cumul = partial_sum_aver(avals)
   pl.plot(range(len(cumul)), cumul)
   pl.title('Ex.2: Partial average against number of proposals, beta = 0.1')
   pl.show()
   print("We will apply a burning procedure to the algorithm. We will discard so the first 200 simulation and we will compute the average and variance of the rest of them in the case beta=0.5.")
   
   new_xvals = xvals[200:]
   mean_new = np.mean(new_xvals)
   variance_new = np.var(new_xvals)
   
   print("The values for mean and average obtained are %f %f"%(mean_new,variance_new) )
   
   pl.hist(new_xvals, bins=75, normed=1)
   pl.title('Ex.2: PDF for the valuse taken after discarding the first 200 simulation.')
   pl.show()

   

   """Case beta = 0.3 """
   xvals,avals = mcmc(expon,myprior,x0,yhat,1000,0.3)
   
   cumul = partial_sum_aver(avals)
   pl.plot(range(len(cumul)), cumul)
   pl.title('Ex.2: Partial average against number of proposals, beta = 0.3')
   pl.show()
   print("We will apply a burning procedure to the algorithm. We will discard so the first 200 simulation and we will compute the average and variance of the rest of them in the case beta=0.5.")
   
   new_xvals = xvals[200:]
   mean_new = np.mean(new_xvals)
   variance_new = np.var(new_xvals)
   
   print("The values for mean and average obtained are %f %f"%(mean_new,variance_new) )
   
   pl.hist(new_xvals, bins=75, normed=1)
   pl.title('Ex.2: PDF for the valuse taken after discarding the first 200 simulation.')
   pl.show()

   

   """Case beta = 0.75 """
   xvals,avals = mcmc(expon,myprior,x0,yhat,1000,0.75)
   
   cumul = partial_sum_aver(avals)
   pl.plot(range(len(cumul)), cumul)
   pl.title('Ex.2: Partial average against number of proposals, beta = 0.75')
   pl.show()
   print("We will apply a burning procedure to the algorithm. We will discard so the first 200 simulation and we will compute the average and variance of the rest of them in the case beta=0.5.")
   
   new_xvals = xvals[200:]
   mean_new = np.mean(new_xvals)
   variance_new = np.var(new_xvals)
   
   print("The values for mean and average obtained are %f %f"%(mean_new,variance_new) )
   
   pl.hist(new_xvals, bins=75, normed=1)
   pl.title('Ex.2: PDF for the valuse taken after discarding the first 200 simulation.')
   pl.show()


   
   """Case beta = 0.65 """
   xvals,avals = mcmc(expon,myprior,x0,yhat,1000,0.65)
   
   cumul = partial_sum_aver(avals)
   pl.plot(range(len(cumul)), cumul)
   pl.title('Ex.2: Partial average against number of proposals, beta = 0.65')
   pl.show()
   print("We will apply a burning procedure to the algorithm. We will discard so the first 200 simulation and we will compute the average and variance of the rest of them in the case beta=0.5.")
   
   new_xvals = xvals[200:]
   mean_new = np.mean(new_xvals)
   variance_new = np.var(new_xvals)
   
   print("The values for mean and average obtained are %f %f"%(mean_new,variance_new) )
   
   pl.hist(new_xvals, bins=75, normed=1)
   pl.title('Ex.2: PDF for the valuse taken after discarding the first 200 simulation.')
   pl.show()


   
   """Case beta = 0.5 """
   xvals,avals = mcmc(expon,myprior,x0,yhat,1000,0.5)
   
   cumul = partial_sum_aver(avals)
   pl.plot(range(len(cumul)), cumul)
   pl.title('Ex.2: Partial average against number of proposals, beta = 0.5')
   pl.show()
   
   
   print("We will apply a burning procedure to the algorithm. We will discard so the first 200 simulation and we will compute the average and variance of the rest of them in the case beta=0.5.")
   
   new_xvals = xvals[200:]
   mean_new = np.mean(new_xvals)
   variance_new = np.var(new_xvals)
   
   print("The values for mean and average obtained are %f %f"%(mean_new,variance_new) )
   
   pl.hist(new_xvals, bins=75, normed=1)
   pl.title('Ex.2: PDF for the valuse taken after discarding the first 200 simulation.')
   pl.show()
   
   
   
   
   
     ##
   ###### EXERCISE 3
     ##
   
   """Case beta = 0.6 """
   xvals,avals = mcmc(square,myprior,x0,yhat,1000,0.6)
   
   cumul = partial_sum_aver(avals)
   pl.plot(range(len(cumul)), cumul)
   pl.title('Ex.3: Partial average against number of proposals, beta = 0.6')
   pl.show()
   print("We will apply a burning procedure to the algorithm. We will discard so the first 300 simulation and we will compute the average and variance of the rest of them in the case beta=0.5.")
   
   new_xvals = xvals[300:] 
   pl.hist(new_xvals, bins=75, normed=1)
   pl.title('Ex.3: PDF for the valuse taken after discarding the first 300 simulation.')
   pl.show()
