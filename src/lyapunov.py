from scipy import integrate
import numpy as np
import pylab
import matplotlib.pyplot as plt

def linear_lorenz_vectorfield(y,t,sigma,rho,beta):
    """
    Function to integrate the lorenz equations together
    with the linearised equations about that trajectory.
    
    inputs:
    y - array containing (x,y,z,dx,dy,dz)
    t - time, not used but needed for the interface
    rho, sigma,beta - parameters
    """

    x = y[:3]
    dx = y[3:]

    #This needs changing
    xdot = np.array([sigma*(x[1]-x[0]),x[0]*(rho - x[2])-x[1],x[0]*x[1] - beta* x[2]])
    dxdot = np.array([sigma*(dx[1]-dx[0]), dx[0]*(rho - x[2])-dx[1]-x[0]*dx[2],dx[0]*x[1] + dx[1]*x[0] - beta* dx[2]])

    #combine xdot and dxdot into one field
    return np.concatenate((xdot,dxdot))

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()

def Phi(x,dx,T):
    """
    Function to integrate the linearised Lorenz equations about
    a given trajectory.
    Inputs:
    x - the initial conditions for the Lorenz trajectory
    dx - the initial conditions for the perturbation
    T - the length of time to integrate for

    Outputs:
    dx - the final value of the perturbation
    """

    y0 = np.concatenate((x,dx))
<<<<<<< HEAD
    #t = np.array([0.,100])
    t = np.arange(0.,T,0.05)
    data = integrate.odeint(linear_lorenz_vectorfield, y0, t=t, args=(10.,28.,8./3.))
    
    return data[:,3:], t

def error_size(dx):

    dist_square = np.square(dx)
    dist_sum = np.sum(dist_square, axis=1)
    euc_dist = np.sqrt(dist_sum)
    return  euc_dist

if __name__ == '__main__':
    mydat , time = Phi([-0.587,-0.563,16.870],[0.01,0.01,0.01],100.0)
    #	plot3D(mydat[0],mydat[1],mydat[2])
    dx_dist = error_size(mydat)
    pylab.semilogy(time,dx_dist)
    pylab.xlabel("time")
    pylab.ylabel("log distance") 

pylab.show()

		
#errgraph = plt.plot(t,err)
    
    
    
=======
    t = np.array([0.,T])
    data = integrate.odeint(linear_lorenz_vectorfield, y0, t=t, args=(10.,28.,8./3.))
>>>>>>> 578a8af5c02c8d9a789553efcd73def671819cd3
    
