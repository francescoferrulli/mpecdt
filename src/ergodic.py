import numpy as np
import pylab
from scipy import integrate
#from data import getdata
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t,args=(10,28,8/3))
    return data, t  

def getdataPar(y0,T,Deltat,sigma=10,rho=28,beta=8/3):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with but now we can move the parameters.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(sigma , rho, beta))
    return data, t 

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
    pylab.title(title)

def ensemble(M,T):
    """
    Function to integrate an ensemble of trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """
    ens = 0.1*np.random.randn(M,3)

    for m in range(M):
        t = np.array([0.,T])
        data = integrate.odeint(vectorfield, 
                                y0=ens[m,:], t=t, args=(10.0,28.,8./3.),
                                mxstep=100000)

        ens[m,:] = data[1,:]

    return ens

def f(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f: - 1d- array of f values
    """

    return d[:,0]**2 + d[:,1]**2 + d[:,2]**2
    
def g(d):
    """
    Secondary function in use in the last question.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f: - 1d- array of f values
    """

    return d[:,0]**3 + d[:,1]**3 + d[:,2]**3
    
def MCapprox_f(ens):
    """
    This function represents the MC approx of the integral of the function f w.r.t. the invariant measure
    """
    a=f(ens)
    f_hat=np.sum(a)/len(a)
    #c= np.sum((a-f_hat)**2)/len(a)**2
    #print c
    return f_hat 
	
def Variance(MCrealisations):
	"""
    Function to calculate the sample-variance 
    """
	mean= np.mean(MCrealisations)
	b= np.sum((MCrealisations - mean)**2)/len(MCrealisations)
	
	return b
"""
The next function will calculate the time-averages.
"""
def average_time(a,tempo):
    av= np.sum(a)/len(tempo)
    return av

if __name__ == '__main__':

    #Y
    
    #print 'Have a look on how 100 very close initial points spread over all the space at time T=0.5,1,10,100 following the flux of the Lorenz system'
    #Ens = ensemble(100, 0.5)
    #Ens1 = ensemble(100, 1)
    #Ens2 = ensemble(100, 10)
    #Ens3 = ensemble(100, 100)
    ##Ens4 = ensemble(100, 500)
    #print 'Plot at time T=0.5'
    #ensemble_plot(Ens,'T=0.5')
    #pylab.show()
    #print 'Plot at time T=1'
    #ensemble_plot(Ens1,'T=1')
    #pylab.show()
    #print 'Plot at time T=10'
    #ensemble_plot(Ens2,'T=10')
    #pylab.show()
    #print 'Plot at time T=100'
    #ensemble_plot(Ens3,'T=100')
    #pylab.show()
    #print 'As we see, after a time T=100 they have almost completely lost their mutual correlation. '
    #print 'Plotting at time T=500 confirm this.'
    #ensemble_plot(Ens4,'T=500')
    #pylab.show()
    #print'These points in some sense are converging to the support of the invariant measure of the Lorenz system.'
    

    ############################################################################################
    """
    The following is to demonstrate that the variance dacays as (M)^-0.5 
    I really suggest to do not compile this part beause it takes ages to end.
    """
    #simulations=30
    #f_hat=np.zeros(simulations)

    #for i in range(simulations):
		#f_hat[i]= MCapprox_f(ensemble(100,50))
    #Var = Variance(f_hat)
    ##Var2 = np.var(f_hat)
    #print 'Variance for 100 simulations for the system run with 100 points evolved at time T=50,  ',Var 
    
    ##f_hat2=np.zeros(100)
    ##for i in range(100):
		##f_hat2[i]= MCapprox_f(ensemble(300,50))
    ##Var_2 = Variance(f_hat2)
    ###Var2_2 = np.var(f_hat2)
    ##print 'Variance for 300 points, 100 simulations ', Var_2 

    #f_hat3=np.zeros(simulations)
    #for i in range(simulations):
		#f_hat3[i]= MCapprox_f(ensemble(400,50))
    #Var_3 = Variance(f_hat3)
    ##Var2_2 = np.var(f_hat2)
    #print 'Variance for 400 points, 100 simulations ', Var_3 
    
    #################################################################################################
        
    ################################################################################################
    #Y
    """
    Thereby there are the time averages for the function f at time T=200 for 4 different initial points.
    """
    
    """
    The following plot show the value of the time average againt N
    """
    evalutime=np.linspace(50.0,1000.0,50)
    averages=np.zeros(len(evalutime))
    for i in range(len(evalutime)):
        averages[i]=  average_time(f(getdata([-0.587,-0.563,16.870],evalutime[i],0.05)[0]),getdata([-0.587,-0.563,16.870],evalutime[i],0.05)[1])
    
    plt.plot(evalutime,averages)
    plt.show()
    
    """
    Now we show that this value is indepent w.r.t. the initial point.
    """
    print 'The following are the time-average for 4 different initial values',[-0.587,-0.563,16.870],[0.587,0.563,-16.870],[-0.587,0.563,16.870],[7.0,2.0,19.0], \
        'for the function f, with T=200. We want to show that this average is independent from the choiche of the initial point.'
    print'Note that we have chosen T=200 and a time step not very small because we want that the points spread over the support of the invariant measure.'
    mydat, mytime = getdata([-0.587,-0.563,16.870],200.0,0.05)
    a=np.zeros(len(mytime))
    a=f(mydat)
    timeaverage = np.sum(a)/len(mytime)
    print timeaverage
    
    mydat1, mytime1 = getdata([0.587,0.563,-16.870],200.0,0.05)
    a1=np.zeros(len(mytime1))
    a1=f(mydat1)
    timeaverage1 = np.sum(a1)/len(mytime1)
    print timeaverage1
    
    mydat2, mytime2 = getdata([-0.587,0.563,16.870],200.0,0.05)
    a2=np.zeros(len(mytime2))
    a2=f(mydat2)
    timeaverage2 = np.sum(a2)/len(mytime2)
    print timeaverage2
    
    mydat3, mytime3 = getdata([0.587,-0.563,-16.870],200.0,0.05)
    a3=np.zeros(len(mytime3))
    a3=f(mydat3)
    timeaverage3 = np.sum(a3)/len(mytime3)
    print timeaverage3
    
    mydat4, mytime4 = getdata([7.0,2.0,19.0],200.0,0.05)
    a4=np.zeros(len(mytime4))
    a4=f(mydat4)
    timeaverage4 = np.sum(a4)/len(mytime4)
    print timeaverage4
    
    print 'The following are the time-average for the same previous initial values, now with T=400'
    
    mydat0, mytime0 = getdata([-0.587,-0.563,16.870],400.0,0.05)
    a0=np.zeros(len(mytime0))
    a0=f(mydat0)
    timeaverage0 = np.sum(a0)/len(mytime0)
    print timeaverage0
    
    mydat01, mytime01 = getdata([0.587,0.563,-16.870],400.0,0.05)
    a01=np.zeros(len(mytime01))
    a01=f(mydat01)
    timeaverage01 = np.sum(a01)/len(mytime01)
    print timeaverage01
    
    mydat02, mytime02 = getdata([-0.587,0.563,0.870],400.0,0.05)
    a02=np.zeros(len(mytime02))
    a02=f(mydat02)
    timeaverage02 = np.sum(a02)/len(mytime02)
    print timeaverage02
    
    mydat03, mytime03 = getdata([10.587,-0.563,-16.870],400.0,0.05)
    a03=np.zeros(len(mytime03))
    a03=f(mydat03)
    timeaverage03 = np.sum(a03)/len(mytime03)
    print timeaverage03
    
    mydat04, mytime04 = getdata([7.0,2.0,19.0],400.0,0.05)
    a04=np.zeros(len(mytime04))
    a04=f(mydat04)
    timeaverage04 = np.sum(a04)/len(mytime04)
    print timeaverage04
    
    print 'It seems that the for values of time bigger than 200 the averege is very tiny sensitive'
    
    print 'End of the exercise number 2'
    
    ####################################################################################################################
    #Question 3
    ####################################################################################################################
    #Y
    print ' '
    print 'Exercise number 3.' 
    print 'We will show how the system changes its quality moving the parameter in a reasonable range. \
              We will see that for some values the system will loose the chaotic behaviour, showing for example spyrals and straight lines.'
    def plot3D(x,y,z):
        """
        Make a 3D plot of data.
        Inputs:
        x - Numpy array of x coordinate
        y - Numpy array of y coordinate
        z - Numpy array of z coordinate
        """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
    
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(x,y,z)
        pylab.show()
    
    #Changes in sigma#
    for i in range(5):

        mydatSigma, mytimeSigma = getdataPar([-0.587,-0.563,16.870],150.0,0.02,10+np.power(-1,i)*i*5,28,8/3)
        
        plot3D(mydatSigma[:,0],mydatSigma[:,1],mydatSigma[:,2])
        pylab.show()

    
    #changes in rho#
    for i in range(5):

        mydatRho, mytimeRho = getdataPar([-0.587,-0.563,16.870],150.0,0.02,10,28+np.power(-1,i)*i*5,8/3)
        
        plot3D(mydatRho[:,0],mydatRho[:,1],mydatRho[:,2])
        pylab.show()

    
    #changes in beta
    for i in range(5):

        mydatBeta, mytimeBeta = getdataPar([-0.587,-0.563,16.870],150.0,0.02,10,28,8/3+np.power(-1,i)*i*5)
        
        plot3D(mydatBeta[:,0],mydatBeta[:,1],mydatBeta[:,2])
        pylab.show()

    print 'Looks as the shape is influenced by changes in all the parameters this because we are changing the quality of the sistem. \
           We have seen behaviours of different kind of system where the chaoticity was lost'

##########################
#Y
    """
    Here we have the last question. We fix small variation in our parameters around the values used before and see how these changements affect the time-average
    """
    variationS= np.linspace(9.5,10.5,100)
    timeaverageVS= np.zeros(100)
    variationR= np.linspace(27.5,28.5,100)
    timeaverageVR= np.zeros(100)
    variationB= np.linspace(8/3 - 0.5,8/3 + 0.5,100)
    timeaverageVB= np.zeros(100)
    #sigma 
    for i in range(100):
        mydatVS, mytime = getdataPar([-0.587,-0.563,16.870],800.0,0.05,variationS[i],28,8/3)
        timeaverageVS[i] = np.average(f(mydatVS))
    plt.plot(variationS,timeaverageVS)
    plt.show()
    print 'The plot under changes in sigma does not show any linear region. This means that the system is really sensitive under small perturbation of sigma.'
    #rho
    for i in range(100):
        mydatVR, mytime = getdataPar([-0.587,-0.563,16.870],800.0,0.05,10,variationR[i],8/3)
        timeaverageVR[i] = np.average(f(mydatVR))
    plt.plot(variationR,timeaverageVR)
    plt.show()
    print 'Now is rho varying and we can spot a linear trend with some noise.'
    #beta
    for i in range(100):
        mydatVB, mytime = getdataPar([-0.587,-0.563,16.870],800.0,0.05,10,28,variationB[i])
        timeaverageVB[i] = np.average(f(mydatVB))
    plt.plot(variationB,timeaverageVB)
    print 'Now is varying beta and even in this can we can spot a linear trend but now the noise seems to be more significant.'
    plt.show()
    
    
    """ We use now a different function, g"""
    
    
    for i in range(100):
        mydatVS, mytime = getdataPar([-0.587,-0.563,16.870],800.0,0.05,variationS[i],28,8/3)
        timeaverageVS[i] = np.average(g(mydatVS))
    plt.plot(variationS,timeaverageVS)
    print 'The plot under changes in sigma does not show any linear region even for the new functio g. This means that the system is really sensitive under small perturbation of sigma.'

    plt.show()
    #rho
    
    for i in range(100):
        mydatVR, mytime = getdataPar([-0.587,-0.563,16.870],800.0,0.05,10,variationR[i],8/3)
        timeaverageVR[i] = np.average(g(mydatVR))
    plt.plot(variationR,timeaverageVR)
    plt.show()
    print ' '
    print 'Now is rho varying and we can spot a linear trend with some noise even with the new function.'
    #beta
    
    for i in range(100):
        mydatVB, mytime = getdataPar([-0.587,-0.563,16.870],800.0,0.05,10,28,variationB[i])
        timeaverageVB[i] = np.average(g(mydatVB))
    plt.plot(variationB,timeaverageVB)
    
    plt.show()
    print ' '
    print 'Now is varying beta and the noise is even more significant than the first case.'
    
